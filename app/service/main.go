package main

import (
	"fmt"
	"github.com/joho/godotenv"
	"gitlab.com/baguss42/go-auto/database/elastic"
	"gitlab.com/baguss42/go-auto/database/pg"
	"gitlab.com/baguss42/go-auto/router"
	"log"
	"net/http"
)

func main() {
	_ = godotenv.Load()

	db := pg.Connect()

	es := elastic.Connect()

	//rabbitmq.Dial()

	r := router.NewRouter(db, es)

	fmt.Println("Server listen at :8080 ...")
	log.Fatal(http.ListenAndServe(":8080", r))
}
