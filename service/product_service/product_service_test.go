package product_service

import (
	"context"
	"errors"
	el "github.com/elastic/go-elasticsearch/v8"
	"github.com/stretchr/testify/suite"
	"gitlab.com/baguss42/go-auto/database/elastic"
	"gitlab.com/baguss42/go-auto/entity"
	"gitlab.com/baguss42/go-auto/repository"
	"gitlab.com/baguss42/go-auto/testdata/fixture"
	mockES "gitlab.com/baguss42/go-auto/testdata/mock/elastic"
	mockRepository "gitlab.com/baguss42/go-auto/testdata/mock/repository"
	"gitlab.com/baguss42/go-auto/utility"
	"gorm.io/gorm"
	"testing"
)

type ProductServiceTestSuite struct {
	suite.Suite
	mockProductRepository *mockRepository.ProductRepositoryMock
	mockESConnection      *mockES.ESConnectionMock
	entityProduct         entity.Product
	productService        ProductService
	ctx                   context.Context
	err                   error
}

func (suite *ProductServiceTestSuite) SetupTest() {
	suite.mockProductRepository = &mockRepository.ProductRepositoryMock{}
	suite.mockESConnection = &mockES.ESConnectionMock{}
	suite.entityProduct = (&fixture.ProductFixture{}).Build()
	suite.productService = ProductService{Repo: suite.mockProductRepository, ES: suite.mockESConnection}
	suite.ctx = context.TODO()
	suite.err = errors.New("unexpected error")
}

func (suite *ProductServiceTestSuite) TestNewProductService() {
	var db *gorm.DB
	var es *el.Client
	expected := &ProductService{Repo: repository.NewProductRepository(db), ES: elastic.NewESConnection(es)}

	result := NewProductService(db, es)
	suite.Equal(result, expected)
}

func (suite *ProductServiceTestSuite) TestCreate() {
	testCases := []struct {
		name        string
		product     entity.Product
		errValidate error
		errRepo     error
		errES       error
	}{
		{
			name:        "Test Case 1 : Success Create Product",
			product:     suite.entityProduct,
			errValidate: nil,
			errRepo:     nil,
			errES:       nil,
		},
		{
			name:        "Test Case 2 : Failed Error Validate Product",
			product:     entity.Product{},
			errValidate: errors.New(entity.ErrFieldIsEmpty("name")),
			errRepo:     nil,
			errES:       nil,
		},
		{
			name:        "Test Case 3 : Failed Error Insert Product",
			product:     suite.entityProduct,
			errValidate: nil,
			errRepo:     suite.err,
			errES:       nil,
		},
	}

	for _, tc := range testCases {
		body := productToReader(tc.product)

		suite.mockProductRepository.On("Insert", suite.ctx, tc.product).Return(tc.product, tc.errRepo).Once()
		suite.mockESConnection.On("ReIndex", suite.ctx, indexName, tc.product.ID, body).Return(tc.errES).Once()

		resultErr := suite.productService.Create(suite.ctx, tc.product)

		if tc.errValidate != nil {
			suite.Equal(tc.errValidate, resultErr)
		} else if tc.errRepo != nil {
			suite.Equal(tc.errRepo, resultErr)
		}
	}
}

func (suite *ProductServiceTestSuite) TestDelete() {
	testCases := []struct {
		name    string
		id      uint
		product entity.Product
		err     error
		errGet  error
	}{
		{
			name:    "Test Case 1 : Success Delete Product",
			id:      1,
			product: suite.entityProduct,
			err:     nil,
			errGet:  nil,
		},
		{
			name:    "Test Case 2 : Failed Delete Product",
			id:      2,
			product: suite.entityProduct,
			err:     suite.err,
			errGet:  nil,
		},
		{
			name:    "Test Case 2 : Failed Delete Product",
			id:      3,
			product: suite.entityProduct,
			err:     nil,
			errGet:  suite.err,
		},
	}

	for _, tc := range testCases {
		body := productToReader(tc.product)

		suite.mockProductRepository.On("Delete", suite.ctx, tc.id).Return(tc.err).Once()
		suite.mockProductRepository.On("Get", suite.ctx, tc.id).Return(tc.product, tc.errGet).Once()
		suite.mockESConnection.On("ReIndex", suite.ctx, indexName, tc.product.ID, body).Return(tc.err).Once()

		resultErr := suite.productService.Delete(suite.ctx, tc.id)

		if tc.errGet != nil {
			suite.Equal(tc.errGet, resultErr)
		} else {
			suite.Equal(tc.err, resultErr)
		}
	}
}

func (suite *ProductServiceTestSuite) TestDetail() {
	testCases := []struct {
		name    string
		id      uint
		product entity.Product
		err     error
	}{
		{
			name:    "Test Case 1 : Success Get Product",
			id:      1,
			product: suite.entityProduct,
			err:     nil,
		},
		{
			name:    "Test Case 2 : Failed Get Product",
			id:      2,
			product: entity.Product{},
			err:     suite.err,
		},
	}

	for _, tc := range testCases {
		suite.mockProductRepository.On("Get", suite.ctx, tc.id).Return(tc.product, tc.err).Once()

		resultProduct, resultErr := suite.productService.Detail(suite.ctx, tc.id)

		suite.Equal(resultProduct, tc.product)
		suite.Equal(resultErr, tc.err)
	}
}

func (suite *ProductServiceTestSuite) TestList() {
	testCases := []struct {
		name     string
		options  utility.ListOptions
		products []entity.Product
		err      error
	}{
		{
			name:     "Test Case 1 : Success Get List",
			options:  utility.NewListOptions(),
			products: []entity.Product{suite.entityProduct},
			err:      nil,
		},
		{
			name:     "Test Case 2 : Failed Get Product",
			options:  utility.NewListOptions(),
			products: []entity.Product{},
			err:      suite.err,
		},
	}

	for _, tc := range testCases {
		suite.mockProductRepository.On("List", suite.ctx, tc.options).Return(tc.products, tc.err).Once()

		resultProducts, resultErr := suite.productService.List(suite.ctx, tc.options)

		suite.Equal(resultProducts, tc.products)
		suite.Equal(resultErr, tc.err)
	}
}

func (suite *ProductServiceTestSuite) TestUpdate() {
	testCases := []struct {
		name        string
		id          uint
		product     entity.Product
		err         error
		errValidate error
		errGet      error
	}{
		{
			name:        "Test Case 1 : Success Update Product",
			id:          1,
			product:     suite.entityProduct,
			err:         nil,
			errValidate: nil,
			errGet:      nil,
		},
		{
			name:        "Test Case 2 : Failed Error Validate Product",
			product:     entity.Product{Name: ""},
			errValidate: errors.New(entity.ErrFieldIsEmpty("name")),
			errGet:      nil,
			err:         nil,
		},
		{
			name:    "Test Case 3 : Failed Update Product",
			id:      3,
			product: suite.entityProduct,
			err:     suite.err,
			errGet:  nil,
		},
		{
			name:    "Test Case 4 : Failed Get Product",
			id:      4,
			product: suite.entityProduct,
			err:     nil,
			errGet:  suite.err,
		},
	}

	for _, tc := range testCases {
		body := productToReader(tc.product)

		suite.mockProductRepository.On("Update", suite.ctx, tc.id, tc.product).Return(tc.err).Once()
		suite.mockProductRepository.On("Get", suite.ctx, tc.id).Return(tc.product, tc.errGet).Once()
		suite.mockESConnection.On("ReIndex", suite.ctx, indexName, tc.product.ID, body).Return(tc.err).Once()

		resultErr := suite.productService.Update(suite.ctx, tc.id, tc.product)

		if tc.err != nil {
			suite.Equal(tc.err, resultErr)
		} else if tc.errGet != nil {
			suite.Equal(tc.errGet, resultErr)
		} else if tc.errValidate != nil {
			suite.Equal(tc.errValidate, resultErr)
		}
	}
}

func TestProductService(t *testing.T) {
	suite.Run(t, new(ProductServiceTestSuite))
}
