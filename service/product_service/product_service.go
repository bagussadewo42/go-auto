package product_service

import (
	"bytes"
	"context"
	"encoding/json"
	"github.com/elastic/go-elasticsearch/v8"
	"gitlab.com/baguss42/go-auto/database/elastic"
	"gitlab.com/baguss42/go-auto/entity"
	"gitlab.com/baguss42/go-auto/repository"
	"gitlab.com/baguss42/go-auto/utility"
	"gorm.io/gorm"
	"io"
)

const indexName = "products"

type ProductServiceInterface interface {
	Create(context.Context, entity.Product) error
	Delete(context.Context, uint) error
	Detail(context.Context, uint) (entity.Product, error)
	List(context.Context, utility.ListOptions) ([]entity.Product, error)
	Update(context.Context, uint, entity.Product) error
}

type ProductService struct {
	Repo repository.ProductRepositoryInterface
	ES   elastic.ESConnectionInterface
}

func NewProductService(db *gorm.DB, es *elasticsearch.Client) *ProductService {
	return &ProductService{
		Repo: repository.NewProductRepository(db),
		ES:   elastic.NewESConnection(es),
	}
}

func (p *ProductService) Create(ctx context.Context, product entity.Product) error {
	err := product.Validate()
	if err != nil {
		return err
	}

	product, err = p.Repo.Insert(ctx, product)
	if err != nil {
		return err
	}

	body := productToReader(product)

	return p.ES.ReIndex(ctx, indexName, product.ID, body)
}

func (p *ProductService) Delete(ctx context.Context, id uint) error {
	err := p.Repo.Delete(ctx, id)
	if err != nil {
		return err
	}

	product, err := p.Repo.Get(ctx, id)
	if err != nil {
		return err
	}

	body := productToReader(product)

	return p.ES.ReIndex(ctx, indexName, product.ID, body)
}

func (p *ProductService) Detail(ctx context.Context, id uint) (entity.Product, error) {
	return p.Repo.Get(ctx, id)
}

func (p *ProductService) List(ctx context.Context, options utility.ListOptions) ([]entity.Product, error) {
	return p.Repo.List(ctx, options)
}

func (p *ProductService) Update(ctx context.Context, id uint, product entity.Product) error {
	err := product.Validate()
	if err != nil {
		return err
	}

	err = p.Repo.Update(ctx, id, product)
	if err != nil {
		return err
	}

	product, err = p.Repo.Get(ctx, id)
	if err != nil {
		return err
	}

	body := productToReader(product)

	return p.ES.ReIndex(ctx, indexName, product.ID, body)
}

func productToReader(product entity.Product) io.Reader {
	res, _ := json.Marshal(product)

	return bytes.NewReader(res)

}
