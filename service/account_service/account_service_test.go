package account_service

import (
	"context"
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"gitlab.com/baguss42/go-auto/entity"
	"gitlab.com/baguss42/go-auto/handler"
	"gitlab.com/baguss42/go-auto/instrument/rabbitmq"
	"gitlab.com/baguss42/go-auto/repository"
	"gitlab.com/baguss42/go-auto/testdata/fixture"
	mockPubSub "gitlab.com/baguss42/go-auto/testdata/mock/rabbitmq"
	mockRepository "gitlab.com/baguss42/go-auto/testdata/mock/repository"
	"gorm.io/gorm"
	"testing"
)

type UserServiceTestSuite struct {
	suite.Suite
	mockUserRepository *mockRepository.AccountRepositoryMock
	mockPubSubService  *mockPubSub.PubSubServiceMock
	entityUser         entity.User
	userService        AccountService
}

func (suite *UserServiceTestSuite) SetupTest() {
	suite.mockUserRepository = &mockRepository.AccountRepositoryMock{}
	suite.mockPubSubService = &mockPubSub.PubSubServiceMock{}
	suite.entityUser = (&fixture.UserFixture{}).Build()
	suite.userService = AccountService{Repo: suite.mockUserRepository, PubSub: suite.mockPubSubService}
}

func (suite *UserServiceTestSuite) TestRegister() {
	testCases := []struct {
		name       string
		entityUser entity.User
		err        error
	}{
		{
			name:       "Test Case 1 | Success Create User",
			entityUser: suite.entityUser,
			err:        nil,
		},
		{
			name:       "Test Case 2 | Failed Create User",
			entityUser: entity.User{},
			err:        errors.New("unexpected error"),
		},
	}

	for _, tc := range testCases {
		suite.mockUserRepository.On("Create", tc.entityUser).Return(tc.entityUser, tc.err).Once()

		suite.mockPubSubService.On("NotifyRegister", tc.entityUser).Return(tc.err).Once()

		resultErr := suite.userService.Register(tc.entityUser)

		suite.Equal(tc.err, resultErr)
	}
}

func (suite *UserServiceTestSuite) TestLogin() {
	testCases := []struct {
		name       string
		email      string
		password   string
		entityUser entity.User
		errLogin   error
		errToken   error
	}{
		{
			name:       "Test Case 1 | Success Login",
			email:      "foo@bar.com",
			password:   "12345",
			entityUser: suite.entityUser,
			errLogin:   nil,
			errToken:   nil,
		},
		{
			name:       "Test Case 2 | Failed Login | User not found",
			email:      "foo@bar.com",
			password:   "1",
			entityUser: entity.User{},
			errLogin:   errors.New("incorrect password"),
			errToken:   nil,
		},
		{
			name:       "Test Case 3| Failed Login | Cannot claim token",
			email:      "foo@bar.com",
			password:   "",
			entityUser: entity.User{},
			errLogin:   nil,
			errToken:   errors.New("could not generate token, id is zero"),
		},
	}

	for _, tc := range testCases {
		suite.mockUserRepository.On("Login", tc.email, tc.password).Return(tc.entityUser, tc.errLogin)

		resultToken, resultError := suite.userService.Login(tc.email, tc.password)

		if tc.errLogin != nil {
			suite.NotNil(resultError)
		} else if tc.errToken != nil {
			suite.NotNil(resultError)
		} else {
			suite.NotNil(resultToken)
		}
	}
}

func (suite *UserServiceTestSuite) TestResendVerifyEmail() {
	testCases := []struct {
		name               string
		email              string
		entityUser         entity.User
		errAccountNotFound error
		errEmailVerified   error
		errPubSub          error
	}{
		{
			name:               "Test Case 1 | Success Resend Verify Email",
			email:              "foo@bar.com",
			entityUser:         entity.User{ID: 1, IsEmailVerified: false},
			errAccountNotFound: nil,
			errEmailVerified:   nil,
			errPubSub:          nil,
		},
		{
			name:               "Test Case 2 | Failed Resend Email | Account Not Found",
			email:              "foo@bar.com",
			entityUser:         entity.User{},
			errAccountNotFound: errors.New("account not found"),
			errEmailVerified:   nil,
			errPubSub:          nil,
		},
		{
			name:               "Test Case 3| Failed Login | Email Already Verified",
			email:              "foo@bar.com",
			entityUser:         entity.User{ID: 1, IsEmailVerified: true},
			errAccountNotFound: nil,
			errEmailVerified:   errors.New("email already verified"),
			errPubSub:          nil,
		},
	}

	for _, tc := range testCases {
		suite.mockUserRepository.On("GetByEmail", tc.email).Return(tc.entityUser).Once()
		suite.mockUserRepository.On("CreateVerification", tc.entityUser).Return(tc.entityUser).Once()
		suite.mockPubSubService.On("NotifyRegister", tc.entityUser).Return(tc.errPubSub).Once()

		resultError := suite.userService.ResendVerifyEmail(tc.email)

		if tc.errAccountNotFound != nil {
			suite.NotNil(resultError)
		} else if tc.errEmailVerified != nil {
			suite.NotNil(resultError)
		} else {
			suite.Nil(resultError)
		}
	}
}

func (suite *UserServiceTestSuite) TestVerifyEmail() {
	var err error
	suite.mockUserRepository.On("VerifyEmailCode", "foo@bar.com", "123").Return(err).Once()

	resultError := suite.userService.VerifyEmail("foo@bar.com", "123")

	suite.Equal(err, resultError)
}

func (suite *UserServiceTestSuite) TestProfile() {
	ctx := context.Background()
	ctx = context.WithValue(ctx, "userID", "1")

	user := suite.entityUser

	userID := handler.GetUserIDFromContext(ctx)

	suite.mockUserRepository.On("GetByID", userID).Return(user).Once()

	resultUser := suite.userService.Profile(ctx)

	suite.Equal(user, resultUser)
}

func TestNewUserService(t *testing.T) {
	var db *gorm.DB
	userService := NewAccountService(db)

	assert.Equal(t, userService, &AccountService{Repo: &repository.AccountRepository{DB: db}, PubSub: &rabbitmq.PubSubService{}})
}

func TestUserService(t *testing.T) {
	suite.Run(t, new(UserServiceTestSuite))
}
