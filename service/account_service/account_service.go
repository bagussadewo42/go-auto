package account_service

import (
	"context"
	"errors"
	"gitlab.com/baguss42/go-auto/entity"
	"gitlab.com/baguss42/go-auto/handler"
	"gitlab.com/baguss42/go-auto/instrument/jwt"
	"gitlab.com/baguss42/go-auto/instrument/rabbitmq"
	"gitlab.com/baguss42/go-auto/repository"
	"gorm.io/gorm"
)

type AccountServiceInterface interface {
	Login(email, password string) (string, error)
	Profile(ctx context.Context) entity.User
	Register(user entity.User) error
	ResendVerifyEmail(email string) error
	VerifyEmail(email, code string) error
}

type AccountService struct {
	Repo   repository.AccountRepositoryInterface
	PubSub rabbitmq.PubSubInterface
}

func NewAccountService(db *gorm.DB) *AccountService {
	return &AccountService{
		Repo:   repository.NewAccountRepository(db),
		PubSub: &rabbitmq.PubSubService{},
	}
}

func (u *AccountService) Login(email, password string) (string, error) {
	user, err := u.Repo.Login(email, password)
	if err != nil {
		return "", err
	}

	token, err := jwt.ClaimToken(uint64(user.ID))
	if err != nil {
		return "", err
	}

	return token, nil
}

func (u *AccountService) Profile(ctx context.Context) entity.User {
	userID := handler.GetUserIDFromContext(ctx)
	user := u.Repo.GetByID(userID)

	return user
}

func (u *AccountService) Register(user entity.User) error {
	user, err := u.Repo.Create(user)
	if err != nil {
		return err
	}

	//_ = u.PubSub.NotifyRegister(user)

	return nil
}

func (u *AccountService) ResendVerifyEmail(email string) error {
	user := u.Repo.GetByEmail(email)
	if user.ID == 0 {
		return errors.New("account not found")
	}

	if user.IsEmailVerified {
		return errors.New("email already verified")
	}

	user = u.Repo.CreateVerification(user)

	//_ = u.PubSub.NotifyRegister(user)

	return nil
}

func (u *AccountService) VerifyEmail(email, code string) error {
	return u.Repo.VerifyEmailCode(email, code)
}
