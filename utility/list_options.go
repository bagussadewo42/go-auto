package utility

import (
	"gitlab.com/baguss42/go-auto/handler"
	"net/url"
)

type ListOptions struct {
	Keyword string
	Limit   int64
	Offset  int64
}

func NewListOptions() ListOptions {
	return ListOptions{
		Keyword: "",
		Limit:   10,
		Offset:  0,
	}
}

func NewListOptionsQueryParams(values url.Values) ListOptions {
	options := NewListOptions()
	options.SetKeyword(handler.GetParamStrByKey(values, "keyword"))
	options.SetLimit(handler.GetParamInt64ByKey(values, "limit"))
	options.SetLimit(handler.GetParamInt64ByKey(values, "limit"))
	return options
}

func (l *ListOptions) SetKeyword(str string) {
	if str == "" {
		return
	}
	l.Keyword = str
}

func (l *ListOptions) SetLimit(limit int64) {
	if limit <= 0 || limit > 100 {
		l.Limit = 10
	} else {
		l.Limit = limit
	}
}

func (l *ListOptions) SetOffset(offset int64) {
	if offset <= 0 {
		l.Offset = 0
	} else {
		l.Offset = offset
	}
}
