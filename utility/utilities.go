package utility

import (
	"crypto/rand"
	"io"
)

const maxCodeLen = 6

func GenerateOTP() string {
	b := make([]byte, maxCodeLen)
	n, err := io.ReadAtLeast(rand.Reader, b, maxCodeLen)
	if n != maxCodeLen {
		panic(err)
	}

	numb := [...]byte{'1', '2', '3', '4', '5', '6', '7', '8', '9', '0'}

	for i := 0; i < len(b); i++ {
		b[i] = numb[int(b[i])%len(numb)]
	}
	return string(b)
}
