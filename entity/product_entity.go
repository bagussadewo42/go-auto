package entity

import (
	"errors"
	"time"
)

type Product struct {
	ID               uint              `json:"id"`
	Name        string                 `json:"name"`
	Description string                 `json:"description"`
	Quantity    int                    `json:"quantity"`
	Price       int64                  `json:"price"`
	Category    map[string]interface{} `json:"category"`
	CreatedAt   time.Time              `json:"created_at,omitempty"`
	UpdatedAt   time.Time              `json:"updated_at,omitempty"`
	IsDeleted   bool                   `json:"is_deleted"`
}

func (p *Product) Validate() error {
	if p.Name == "" {
		return errors.New(ErrFieldIsEmpty("name"))
	}
	if p.Quantity <= 0 {
		return errors.New(ErrFieldIsZeroOrNegative("quantity"))
	}
	if p.Price <= 0 {
		return errors.New(ErrFieldIsZeroOrNegative("price"))
	}
	if len(p.Description) < 15 {
		return errors.New(ErrFieldIsLessLenght("description", 15))
	}
	return nil
}
