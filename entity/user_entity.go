package entity

import (
	"database/sql"
	"golang.org/x/crypto/bcrypt"
	"log"
	"time"
)

type User struct {
	ID               uint              `json:"id"`
	FirstName        string            `json:"first_name"`
	LastName         string            `json:"last_name"`
	Email            string            `json:"email"`
	IsEmailVerified  bool              `json:"-" gorm:"is_email_verified"`
	Phone            string            `json:"phone"`
	Password         string            `json:"password,omitempty" gorm:"-:all" `
	PasswordHash     []byte            `json:"-" gorm:"password_hash"`
	Role             string            `json:"-" gorm:"-"`
	LastLoginAt      sql.NullTime      `json:"last_login_at" gorm:"last_login_at"`
	UserVerification *UserVerification `json:",omitempty" gorm:"-:all"`
}

type UserVerification struct {
	ID               uint
	UserID           uint
	Email            string
	VerificationCode string
	ExpiredAt        time.Time
}

func (u *User) HashPassword() {
	if u.Password == "" {
		return
	}
	pwd, err := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.MinCost)
	if err != nil {
		panic("could not encrypt password")
	}

	u.PasswordHash = pwd
}

func (u *User) VerifyPassword(password string) bool {
	if err := bcrypt.CompareHashAndPassword(u.PasswordHash, []byte(password)); err != nil {
		log.Println(err)
		return false
	}

	return true
}
