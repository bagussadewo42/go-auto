package entity

import (
	"errors"
	"fmt"
)

const (
	RecordNotFound = "record not found"
)

var (
	ErrRecordNotFound = errors.New(RecordNotFound)
)

func ErrFieldIsEmpty(field string) string {
	return fmt.Sprintf("field %s should not be empty", field)
}

func ErrFieldIsZeroOrNegative(field string) string {
	return fmt.Sprintf("field %s should not be zero or negative", field)
}

func ErrFieldIsLessLenght(field string, length int) string {
	return fmt.Sprintf("field %s characters is less than %v characters", field, length)
}
