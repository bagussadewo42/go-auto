package router

import (
	"github.com/elastic/go-elasticsearch/v8"
	"github.com/julienschmidt/httprouter"
	"gitlab.com/baguss42/go-auto/handler"
	"gitlab.com/baguss42/go-auto/handler/account_handler"
	"gitlab.com/baguss42/go-auto/handler/product_handler"
	"gitlab.com/baguss42/go-auto/instrument/prometheus"
	"gitlab.com/baguss42/go-auto/middleware"
	"gitlab.com/baguss42/go-auto/service/account_service"
	"gitlab.com/baguss42/go-auto/service/product_service"
	"gorm.io/gorm"
	"net/http"
)

func NewRouter(db *gorm.DB, es *elasticsearch.Client) *httprouter.Router {
	r := httprouter.New()

	middleware.Load()

	r.GET("/", middleware.Apply(handler.Index))
	r.GET("/healthz", middleware.Apply(handler.Index))
	r.HandlerFunc(http.MethodGet, "/metrics", prometheus.Handler)

	accountHandler := account_handler.AccountHandler{Service: account_service.NewAccountService(db)}
	r.POST("/api/account/register", middleware.Apply(accountHandler.Register))
	r.POST("/api/account/verify/email", middleware.Apply(accountHandler.VerifyEmail))
	r.POST("/api/account/resend_verify/email", middleware.Apply(accountHandler.ResendVerifyEmail))
	r.POST("/api/account/login", middleware.Apply(accountHandler.Login))
	r.GET("/api/account/profile", middleware.Apply(middleware.Auth(accountHandler.Profile)))
	r.POST("/api/account/logout", middleware.Apply(middleware.Auth(accountHandler.Logout)))

	productHandler := product_handler.ProductHandler{Service: product_service.NewProductService(db, es)}
	r.POST("/api/product/create", middleware.Apply(middleware.Auth(productHandler.Create)))
	r.DELETE("/api/product/delete/:id", middleware.Apply(middleware.Auth(productHandler.Delete)))
	r.GET("/api/product/get/:id", middleware.Apply(middleware.Auth(productHandler.Detail)))
	r.GET("/api/product/get", middleware.Apply(middleware.Auth(productHandler.List)))
	r.PUT("/api/product/update/:id", middleware.Apply(middleware.Auth(productHandler.Update)))

	return r
}
