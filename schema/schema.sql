CREATE TABLE IF NOT EXISTS "users" (
  "id" SERIAL PRIMARY KEY,
  "username" varchar(255),
  "first_name" varchar(255) NOT NULL,
  "last_name" varchar(255),
  "email" varchar(255) UNIQUE NOT NULL,
  "is_email_verified" bool default false,
  "phone" varchar(50),
  "password_hash" bytea NOT NULL,
  "role" varchar(10) default 'buyer' NOT NULL,
  "last_login_at" timestamp,
  "created_at" timestamp DEFAULT (now()),
  "updated_at" timestamp
);

CREATE INDEX "users_index_username" ON "users" ("username");
CREATE INDEX "users_index_email" ON "users" ("email");

CREATE TABLE IF NOT EXISTS "user_verifications" (
    "id" SERIAL PRIMARY KEY,
    "user_id" SERIAL,
    "email" varchar(255),
    "verification_code" varchar(100),
    "expired_at" timestamp
);

ALTER TABLE "user_verifications" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id");
CREATE INDEX "user_verifications_index_email" ON "user_verifications" ("email");

CREATE TABLE IF NOT EXISTS "addresses" (
  "id" SERIAL PRIMARY KEY,
  "user_id" SERIAL,
  "name" varchar(255),
  "address" varchar(255),
  "hamlet" varchar(255),
  "districts" varchar(255),
  "province" varchar(255),
  "created_at" timestamp default (now()),
  "updated_at" timestamp
);

ALTER TABLE "addresses" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id");

CREATE TABLE IF NOT EXISTS "products" (
    "id" SERIAL PRIMARY KEY,
    "name" varchar(250) NOT NULL UNIQUE,
    "description" text,
    "category_id" SERIAL,
    "car_brand_id" SERIAL,
    "car_model_id" SERIAL,
    "car_year" int,
    "qty" int,
    "price" bigint,
    "price_reduction" bigint,
    "is_active" bool default true,
    "created_at" timestamp default (now()),
    "created_by" SERIAL,
    "updated_at" timestamp,
    "updated_by" SERIAL
);

ALTER TABLE "products" ADD FOREIGN KEY ("created_by") REFERENCES "users" ("id");
ALTER TABLE "products" ADD FOREIGN KEY ("updated_by") REFERENCES "users" ("id");
CREATE INDEX "products_index_is_active" ON "products" ("is_active");
CREATE INDEX "products_index_category_id" ON "products" ("category_id");
CREATE INDEX "products_index_car_model_id" ON "products" ("car_model_id");

CREATE TABLE IF NOT EXISTS "categories" (
    "id" SERIAL PRIMARY KEY,
    "parent_category_id" SERIAL,
    "name" varchar(255),
    "is_active" bool default true,
    "created_at" timestamp default (now()),
    "created_by" SERIAL,
    "updated_at" timestamp,
    "updated_by" SERIAL
);

ALTER TABLE "categories" ADD FOREIGN KEY ("created_by") REFERENCES "users" ("id");
ALTER TABLE "categories" ADD FOREIGN KEY ("updated_by") REFERENCES "users" ("id");
CREATE INDEX "categories_index_parent_category_id" ON "categories" ("parent_category_id");
CREATE INDEX "categories_index_is_active" ON "categories" ("is_active");

CREATE TABLE IF NOT EXISTS "car_brands" (
    "id" SERIAL PRIMARY KEY,
    "name" varchar(255),
    "category_id" SERIAL,
    "logo_url" varchar(255),
    "is_active" bool default true,
    "created_at" timestamp default (now()),
    "created_by" SERIAL,
    "updated_at" timestamp,
    "updated_by" SERIAL
);

ALTER TABLE "car_brands" ADD FOREIGN KEY ("category_id") REFERENCES "categories" ("id");
ALTER TABLE "car_brands" ADD FOREIGN KEY ("created_by") REFERENCES "users" ("id");
ALTER TABLE "car_brands" ADD FOREIGN KEY ("updated_by") REFERENCES "users" ("id");
CREATE INDEX "car_brands_index_category_id" ON "car_brands" ("category_id");
CREATE INDEX "car_brands_index_is_active" ON "car_brands" ("is_active");

CREATE TABLE IF NOT EXISTS "car_models" (
    "id" SERIAL PRIMARY KEY,
    "name" varchar(255),
    "car_brand_id" SERIAL,
    "logo_url" varchar(255),
    "is_active" bool default true,
    "created_at" timestamp default (now()),
    "created_by" SERIAL,
    "updated_at" timestamp,
    "updated_by" SERIAL
);

ALTER TABLE "car_models" ADD FOREIGN KEY ("car_brand_id") REFERENCES "car_brands" ("id");
ALTER TABLE "car_models" ADD FOREIGN KEY ("created_by") REFERENCES "users" ("id");
ALTER TABLE "car_models" ADD FOREIGN KEY ("updated_by") REFERENCES "users" ("id");
CREATE INDEX "car_models_index_is_active" ON "car_models" ("is_active");

CREATE TABLE IF NOT EXISTS "transactions" (
    "id" SERIAL PRIMARY KEY,
    "date" timestamp default now(),
    "transaction_number" varchar(255),
    "buyer_id" SERIAL,
    "total_amount" bigint,
    "reduction_amount" bigint,
    "delivery_amount" bigint,
    "coded_amount" bigint, -- for payment code
    "tax_amount" bigint,
    "payment_method" varchar(50),
    "courier_id" SERIAL,
    "courier_package_id" SERIAL,
    "state" varchar(10), -- pending, paid, accepted, cancel, delivered, remitted, refunded
    "paid_at" timestamp,
    "accepted_at" timestamp,
    "canceled_at" timestamp,
    "cancellation_notes" text,
    "delivered_at" timestamp,
    "remitted_at" timestamp,
    "refunded_at" timestamp,
    "is_active" bool default true,
    "created_at" timestamp default (now()),
    "created_by" SERIAL,
    "updated_at" timestamp,
    "updated_by" SERIAL
)