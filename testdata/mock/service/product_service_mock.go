package mock_service

import (
	"context"
	"github.com/stretchr/testify/mock"
	"gitlab.com/baguss42/go-auto/entity"
	"gitlab.com/baguss42/go-auto/utility"
)

type ProductServiceMock struct {
	mock.Mock
}

func (p *ProductServiceMock) Create(ctx context.Context, product entity.Product) error {
	result := p.Called(ctx, product)
	return result.Error(0)
}

func (p *ProductServiceMock) Delete(ctx context.Context, id uint) error {
	result := p.Called(ctx, id)
	return result.Error(0)
}

func (p *ProductServiceMock) Detail(ctx context.Context, id uint) (entity.Product, error) {
	result := p.Called(ctx, id)
	return result.Get(0).(entity.Product), result.Error(1)
}

func (p *ProductServiceMock) List(ctx context.Context, options utility.ListOptions) ([]entity.Product, error) {
	result := p.Called(ctx, options)
	return result.Get(0).([]entity.Product), result.Error(1)
}

func (p *ProductServiceMock) Update(ctx context.Context, id uint, product entity.Product) error {
	result := p.Called(ctx, id, product)
	return result.Error(0)
}
