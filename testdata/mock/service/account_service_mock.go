package mock_service

import (
	"context"
	"github.com/stretchr/testify/mock"
"gitlab.com/baguss42/go-auto/entity"
)

type AccountServiceMock struct {
	mock.Mock
}

func (m *AccountServiceMock) Login(email, password string) (string, error) {
	args := m.Called(email, password)
	return args.Get(0).(string), args.Error(1)
}

func (m *AccountServiceMock) Profile(ctx context.Context) entity.User {
	args := m.Called(ctx)
	return args.Get(0).(entity.User)
}

func (m *AccountServiceMock) Register(user entity.User) error {
	args := m.Called(user)
	return args.Error(0)
}

func (m *AccountServiceMock) ResendVerifyEmail(email string) error {
	args := m.Called(email)
	return args.Error(0)
}

func (m *AccountServiceMock) VerifyEmail(email, code string) error {
	args := m.Called(email, code)
	return args.Error(0)
}