package rabbitmq

import (
	"github.com/stretchr/testify/mock"
	"gitlab.com/baguss42/go-auto/entity"
)

type PubSubServiceMock struct {
	mock.Mock
}

func (m *PubSubServiceMock) NotifyRegister(user entity.User) error {
	args := m.Called(user)
	return args.Error(0)
}
