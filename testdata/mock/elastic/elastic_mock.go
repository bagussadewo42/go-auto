package elastic

import (
	"context"
	"github.com/stretchr/testify/mock"
	"io"
)

type ESConnectionMock struct {
	mock.Mock
}

func (es *ESConnectionMock) ReIndex(ctx context.Context, index string, documentID uint, body io.Reader) error {
	args := es.Called(ctx, index, documentID, body)
	return args.Error(0)
}