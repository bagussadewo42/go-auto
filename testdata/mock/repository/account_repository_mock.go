package mock_repository

import (
	"github.com/stretchr/testify/mock"
	"gitlab.com/baguss42/go-auto/entity"
)

type AccountRepositoryMock struct {
	mock.Mock
}

func (m *AccountRepositoryMock) Create(user entity.User) (entity.User, error) {
	args := m.Called(user)
	return args.Get(0).(entity.User), args.Error(1)
}

func (m *AccountRepositoryMock) CreateVerification(user entity.User) entity.User {
	args := m.Called(user)
	return args.Get(0).(entity.User)
}

func (m *AccountRepositoryMock) Login(email, password string) (entity.User, error) {
	args := m.Called(email, password)
	return args.Get(0).(entity.User), args.Error(1)
}

func (m *AccountRepositoryMock) GetByID(ID uint) entity.User {
	args := m.Called(ID)
	return args.Get(0).(entity.User)
}

func (m *AccountRepositoryMock) GetByEmail(email string) entity.User {
	args := m.Called(email)
	return args.Get(0).(entity.User)
}

func (m *AccountRepositoryMock) VerifyEmailCode(email, code string) error {
	args := m.Called(email, code)
	return  args.Error(0)
}