package mock_repository

import (
	"context"
	"github.com/stretchr/testify/mock"
	"gitlab.com/baguss42/go-auto/entity"
	"gitlab.com/baguss42/go-auto/utility"
)

type ProductRepositoryMock struct {
	mock.Mock
}

func (m *ProductRepositoryMock) Delete(ctx context.Context, id uint) error {
	args := m.Called(ctx, id)
	return args.Error(0)
}

func (m *ProductRepositoryMock) Get(ctx context.Context, id uint) (entity.Product, error) {
	args := m.Called(ctx, id)
	return args.Get(0).(entity.Product), args.Error(1)
}

func (m *ProductRepositoryMock) Insert(ctx context.Context, product entity.Product) (entity.Product, error) {
	args := m.Called(ctx, product)
	return args.Get(0).(entity.Product), args.Error(1)
}

func (m *ProductRepositoryMock) List(ctx context.Context, options utility.ListOptions) ([]entity.Product, error) {
	args := m.Called(ctx, options)
	return args.Get(0).([]entity.Product), args.Error(1)
}

func (m *ProductRepositoryMock) Update(ctx context.Context,id uint,product entity.Product) error {
	args := m.Called(ctx, id, product)
	return args.Error(0)
}