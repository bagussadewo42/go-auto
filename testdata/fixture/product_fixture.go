package fixture

import (
	"gitlab.com/baguss42/go-auto/entity"
)

type ProductFixture struct {
	entity.Product
}

func (p *ProductFixture) Build() entity.Product {
	return entity.Product{
		Name: "Anything",
		Description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
		Quantity: 10,
		Price: 50000,
		Category: map[string]interface{}{
			"car": "Toyota",
			"is_sale": true,
		},
	}
}
