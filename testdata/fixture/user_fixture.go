package fixture

import "gitlab.com/baguss42/go-auto/entity"

type UserFixture struct {
	entity.User
}

func(u *UserFixture) Build() entity.User {
	return entity.User{
		ID: 1,
		FirstName: "foo",
		LastName: "bar",
		Email: "foo@bar.com",
		IsEmailVerified: false,
		Password: "12345",
	}
}