from locust import HttpUser, TaskSet, task
import random
import string


def random_str():
    return ''.join(random.choices(string.ascii_lowercase, k=10))


class AccountHandler(HttpUser):
    @task()
    def register(self):
        self.client.post("/api/account/register",
                         json={
                             "first_name": random_str(),
                             "last_name": random_str(),
                             "email": random_str() + "@gmail.com",
                             "password": random_str()
                         }
                         )

    @task()
    def login(self):  # TODO: should register with this account first
        self.client.post("/api/account/login",
                         data={
                             "email": "bagussadewo42@gmail.com",
                             "password": "12345"
                         }
                         )

    @task()
    def profile(self):  # TODO: get jwt from login with registered account
        self.client.get("/api/account/profile",
                        headers={
                            "Cookie": "user-jwt=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2NDk0NjQ5NDYsImlzcyI6IjIzOTA0In0.BNlIow7HWUnM9fZMP0FahlojHgu3bhKuDhwtprochyc"}
                        )
