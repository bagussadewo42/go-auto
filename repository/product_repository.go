package repository

import (
	"context"
	"gitlab.com/baguss42/go-auto/entity"
	"gitlab.com/baguss42/go-auto/utility"
	"gorm.io/gorm"
)

type ProductRepositoryInterface interface {
	Delete(context.Context, uint) error
	Get(context.Context, uint) (entity.Product, error)
	Insert(context.Context, entity.Product) (entity.Product, error)
	List(context.Context, utility.ListOptions) ([]entity.Product, error)
	Update(context.Context, uint, entity.Product) error
}

type ProductRepository struct {
	DB *gorm.DB
}

func NewProductRepository(db *gorm.DB) *ProductRepository {
	return &ProductRepository{
		DB: db,
	}
}

func (p *ProductRepository) Delete(ctx context.Context, id uint) error {
	return nil
}

func (p *ProductRepository) Get(ctx context.Context, id uint) (product entity.Product, err error) {
	return product, err
}

func (p *ProductRepository) Insert(ctx context.Context, product entity.Product) (entity.Product, error) {
	return product, nil
}

func (p *ProductRepository) List(ctx context.Context, opts utility.ListOptions) (products []entity.Product, err error) {
	return
}

func (p *ProductRepository) Update(ctx context.Context, id uint, product entity.Product) error {
 return nil
}
