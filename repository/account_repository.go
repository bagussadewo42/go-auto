package repository

import (
	"errors"
	"gitlab.com/baguss42/go-auto/database/pg"
	"gitlab.com/baguss42/go-auto/entity"
	"gitlab.com/baguss42/go-auto/utility"
	"gorm.io/gorm"
	"time"
)

const (
	users                   = "users"
	userVerifications       = "user_verifications"
	ErrUnexpectedCreateUser = "could not create user"
	ErrUniqueEmail          = "email already registered"
	ErrNotFoundEmail        = "email not registered"
	ErrIncorrectPassword    = "incorrect password"
)

type AccountRepositoryInterface interface {
	Create(user entity.User) (entity.User, error)
	CreateVerification(user entity.User) entity.User
	GetByID(uint) entity.User
	GetByEmail(email string) entity.User
	Login(email, password string) (entity.User, error)
	VerifyEmailCode(email, code string) error
}

type AccountRepository struct{
	DB *gorm.DB
}

func NewAccountRepository(db *gorm.DB) *AccountRepository {
	return &AccountRepository{
		DB: db,
	}
}

func (u *AccountRepository) Create(user entity.User) (entity.User, error) {
	user.HashPassword()

	if err := u.DB.Table(users).Create(&user).Error; err != nil {
		if pg.IsErrorDuplicate(err) {
			return entity.User{}, errors.New(ErrUniqueEmail)
		} else {
			return entity.User{}, errors.New(ErrUnexpectedCreateUser)
		}
	}

	user = u.CreateVerification(user)

	return user, nil
}

func (u *AccountRepository) GetByID(userID uint) (user entity.User) {
	if userID <= 0 {
		return
	}
	u.DB.Table(users).Where("id = ?", userID).First(&user)
	return
}

func (u *AccountRepository) GetByEmail(email string) (user entity.User) {
	u.DB.Table(users).Where("email = ?", email).First(&user)
	return user
}

func (u *AccountRepository) Login(email, password string) (entity.User, error) {
	var user entity.User

	user = u.GetByEmail(email)
	if user.ID == 0 {
		return user, errors.New(ErrNotFoundEmail)
	}

	if !user.VerifyPassword(password) {
		return user, errors.New(ErrIncorrectPassword)
	}

	u.UpdateLoginAt(user)

	return user, nil
}

func (u *AccountRepository) VerifyEmailCode(email, code string) error {
	userVerification := entity.UserVerification{}

	err := u.DB.Table(userVerifications).Where("email = ? AND verification_code = ? AND ? <= expired_at", email, code, time.Now()).Last(&userVerification).Error
	if err != nil {
		return err
	}

	u.UpdateVerifiedEmailTrue(email)

	return nil
}

func (u *AccountRepository) CreateVerification(user entity.User) entity.User {
	userVerification := entity.UserVerification{
		Email:            user.Email,
		UserID:           user.ID,
		VerificationCode: utility.GenerateOTP(),
		ExpiredAt:        time.Now().Add(10 * time.Minute),
	}

	u.DB.Create(&userVerification)

	user.UserVerification = &userVerification

	return user
}

func (u *AccountRepository) UpdateVerifiedEmailTrue(email string) {
	u.DB.Table(users).Where("email = ?", email).Update("is_email_verified", true)
}

func (u *AccountRepository) UpdateLoginAt(user entity.User) {
	u.DB.Model(user).Update("last_login_at", time.Now())
}
