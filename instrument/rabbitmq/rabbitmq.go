package rabbitmq

import (
	"encoding/json"
	"github.com/streadway/amqp"
	"gitlab.com/baguss42/go-auto/entity"
	"log"
	"os"
)

var PubSub *amqp.Connection

const registerChannel = "NotifyRegister"

type PubSubInterface interface {
	NotifyRegister(user entity.User) error
}

type PubSubService struct {
}

func Dial() {
	host := os.Getenv("PUBSUB_HOST")
	connRabbitMQ, err := amqp.Dial(host)
	if err != nil {
		log.Fatal(err)
	}

	PubSub = connRabbitMQ
}

func (p *PubSubService) NotifyRegister(user entity.User) error {
	channRabbitMQ, err := PubSub.Channel()
	if err != nil {
		return err
	}
	defer channRabbitMQ.Close()

	_, err = channRabbitMQ.QueueDeclare(
		registerChannel,
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		return err
	}

	userByte, err := json.Marshal(user)
	if err != nil {
		return err
	}

	message := amqp.Publishing{
		ContentType: "application/json",
		Body:        userByte,
	}

	if err = channRabbitMQ.Publish(
		"",
		registerChannel,
		false,
		false,
		message,
	); err != nil {
		return err
	}

	return nil
}
