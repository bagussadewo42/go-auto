package jwt

import (
	"errors"
	"github.com/dgrijalva/jwt-go"
	"os"
	"strconv"
	"time"
)

const COOKIE_NAME_JWT = "user-jwt"

func ClaimToken(userID uint64) (string, error) {
	if userID == 0 {
		return "", errors.New("could not generate token, id is zero")
	}

	claims := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.StandardClaims{
		Issuer:    strconv.FormatUint(userID, 10),
		ExpiresAt: time.Now().Add(time.Hour * 24).Unix(), // 1 Day expired
	})

	rsaKey := os.Getenv("RSA_KEY")
	token, err := claims.SignedString([]byte(rsaKey))

	if err != nil {
		return "", err
	}

	return token, nil
}

func VerifyToken(tokenString string) (userID string, err error) {
	token, err := jwt.ParseWithClaims(tokenString, &jwt.StandardClaims{}, func(token *jwt.Token) (interface{}, error) {
		rsaKey := os.Getenv("RSA_KEY")
		return []byte(rsaKey), nil
	})

	if err != nil {
		return
	}

	claims := token.Claims.(*jwt.StandardClaims)

	return claims.Issuer, nil
}
