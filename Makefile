PROJECT_NAME = go-auto
CI_REGISTRY ?= baguss42
IMAGE = $(CI_REGISTRY)/$(PROJECT_NAME)
ODIR := deploy/_output

export VERSION ?= $(shell git show -q --format=%h)

run:
	go run app/service/main.go

test:
	go test -cover -coverprofile=cover.out ./...

goverall:
	go get github.com/mattn/goveralls@v0.0.3
	goveralls -repotoken=${COVERALLS_REPO_TOKEN} -coverprofile=cover.out -service=gitlab-cloud

compile:
	GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -o $(ODIR)/service/service app/service/main.go

build:
	docker build -t $(IMAGE):$(VERSION) -f Dockerfile .

push:
	docker tag $(IMAGE):$(VERSION) $(IMAGE):$(VERSION)
	docker push $(IMAGE):$(VERSION)

up: compile
	docker-compose --file docker-compose.yml --env-file docker-compose.env up -d

down:
	docker-compose --file docker-compose.yml --env-file docker-compose.env down

dev:
	docker-compose --file docker-compose-dev.yml --env-file docker-compose-dev.env up -d

stop:
	docker-compose --file docker-compose-dev.yml --env-file docker-compose-dev.env down

fmt:
	go fmt ./...

coverage-report: test
	go tool cover -html=cover.out -o=coverage.html

deployment-apply:
	kubectl apply -f ./deployment/

deployment-delete:
	kubectl delete -f ./deployment/