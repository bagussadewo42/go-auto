package middleware

import (
	"context"
	"errors"
	"github.com/julienschmidt/httprouter"
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/baguss42/go-auto/handler"
	"gitlab.com/baguss42/go-auto/instrument/jwt"
	p "gitlab.com/baguss42/go-auto/instrument/prometheus"
	"go.uber.org/zap"
	"net/http"
	"strconv"
	"sync"
	"time"
)

type HandleWithError func(http.ResponseWriter, *http.Request, httprouter.Params) (int, error)

const ErrUnauthorized = "unauthorized"

var (
	logger   *zap.Logger
	prom     *prometheus.HistogramVec
	promOnce sync.Once
)

func Load() {
	l, _ := zap.NewProduction()
	logger = l

	prom = p.ServiceLatencyHistogram()
	promOnce.Do(func() {
		p.RegisterMetrics(prom)
	})
}

func Apply(handle HandleWithError) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
		start := time.Now()

		httpCode, err := handle(w, r, params)

		elapsed := time.Since(start).Seconds() * 1000
		elapsedStr := strconv.FormatFloat(elapsed, 'f', -1, 64)

		if err != nil {
			logger.Error(err.Error(),
				zap.String("url", r.URL.String()),
				zap.String("path", r.URL.Path),
				zap.String("method", r.Method),
				zap.String("type", "info"),
				zap.String("duration", elapsedStr),
				zap.Int("http_status", httpCode),
			)
			prom.WithLabelValues(r.Method, r.URL.Path, "fail").Observe(elapsed)
		} else {
			logger.Info("everything ok",
				zap.String("url", r.URL.String()),
				zap.String("path", r.URL.Path),
				zap.String("method", r.Method),
				zap.String("type", "info"),
				zap.String("duration", elapsedStr),
				zap.Int("http_status", httpCode),
			)
			prom.WithLabelValues(r.Method, r.URL.Path, "ok").Observe(elapsed)
		}
	}
}

// None is empty middleware, usually for testing usage
func None(handle HandleWithError) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
		_, _ = handle(w, r, params)
	}
}

func Auth(handle HandleWithError) HandleWithError {
	return func(w http.ResponseWriter, r *http.Request, params httprouter.Params) (int, error) {
		jwtCookie, err := r.Cookie(jwt.COOKIE_NAME_JWT)
		if err != nil {
			return handler.WriteUnauthorizedError(w, errors.New(ErrUnauthorized))

		}

		userID, err := jwt.VerifyToken(jwtCookie.Value)
		if err != nil {
			return handler.WriteUnauthorizedError(w, errors.New(ErrUnauthorized))
		}

		ctx := r.Context()

		ctx = context.WithValue(ctx, "userID", userID)

		return handle(w, r.WithContext(ctx), params)
	}
}
