package pg

import (
	"fmt"
	"github.com/jackc/pgconn"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"log"
	"os"
)

const (
	UniqueViolationErr = "23505" // UniqueViolationErr for unique duplicate key
)

// Connect create connection with postgresql database
func Connect() *gorm.DB {
	pgHost := os.Getenv("POSTGRES_HOST")
	pgUser := os.Getenv("POSTGRES_USER")
	pgPassword := os.Getenv("POSTGRES_PASSWORD")
	pgDB := os.Getenv("POSTGRES_DB")
	pgPort := os.Getenv("POSTGRES_PORT")

	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=disable TimeZone=Asia/Jakarta", pgHost, pgUser, pgPassword, pgDB, pgPort)
	connection, err := gorm.Open(postgres.Open(dsn), &gorm.Config{
		Logger: logger.Default.LogMode(logger.Silent),
	})

	if err != nil {
		panic("could not connect to the database")
	}

	log.Println("Postgre connected ...")

	return connection
}

// IsErrorDuplicate to check if error is unique duplicate key
func IsErrorDuplicate(err error) bool {
	if pgErr, ok := err.(*pgconn.PgError); ok {
		return pgErr.Code == UniqueViolationErr
	}
	return false
}
