package elastic

import (
	"context"
	"errors"
	el "github.com/elastic/go-elasticsearch/v8"
	"github.com/elastic/go-elasticsearch/v8/esapi"
	"io"
	"log"
	"strconv"
)

type ESConnectionInterface interface {
	ReIndex(Context context.Context, Index string, DocumentID uint, DocumentBody io.Reader) error
}

type ESConnection struct {
	Client *el.Client
}

func NewESConnection(client *el.Client) *ESConnection {
	return &ESConnection{
		Client: client,
	}
}

func Connect() *el.Client {
	config := el.Config{
		Username: "elastic",
		Password: "go-auto_elastic",
	}
	client, err := el.NewClient(config)
	if err != nil {
		log.Fatalf("could not connect elastic: %v", err)
	}
	log.Println(client.Ping())
	return client
}

func (e *ESConnection) ReIndex(ctx context.Context, index string, documentID uint, body io.Reader) error {
	if index == "" {
		return errors.New("index is empty")
	}
	if documentID == 0 {
		return errors.New("documentID is empty")
	}
	if body == nil {
		return errors.New("body is empty")
	}
	documentIDStr := strconv.FormatUint(uint64(documentID), 10)
	req := esapi.IndexRequest{
		Index:      index,
		DocumentID: documentIDStr,
		Body:       body,
		Refresh:    "true",
	}

	res, err := req.Do(ctx, e.Client)
	if res.IsError() || err != nil {
		return errors.New(res.String())
	}

	return res.Body.Close()
}
