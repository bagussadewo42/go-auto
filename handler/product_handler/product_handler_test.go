package product_handler

import (
	"encoding/json"
	"errors"
	"github.com/julienschmidt/httprouter"
	"github.com/stretchr/testify/suite"
	"gitlab.com/baguss42/go-auto/entity"
	"gitlab.com/baguss42/go-auto/handler"
	"gitlab.com/baguss42/go-auto/middleware"
	"gitlab.com/baguss42/go-auto/repository"
	"gitlab.com/baguss42/go-auto/testdata/fixture"
	mockService "gitlab.com/baguss42/go-auto/testdata/mock/service"
	"gitlab.com/baguss42/go-auto/utility"
	"io"
	"log"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

type ProductHandlerTestSuite struct {
	suite.Suite
	mockProductService *mockService.ProductServiceMock
	productHandler     ProductHandler
	productEntity      entity.Product
	productByte        []byte
}

func (suite *ProductHandlerTestSuite) SetupTest() {
	suite.mockProductService = &mockService.ProductServiceMock{}
	suite.productEntity = (&fixture.ProductFixture{}).Build()
	suite.productHandler = ProductHandler{Service: suite.mockProductService}

	var err error
	suite.productByte, err = json.Marshal(suite.productEntity)
	if err != nil {
		log.Fatalf("could not encode product")
	}
	middleware.Load()
}

func (suite *ProductHandlerTestSuite) TestDecodeUser() {
	bodyRequest := strings.NewReader(string(suite.productByte))
	user, err := decodeProduct(bodyRequest)
	suite.Equal(user, suite.productEntity)
	suite.Nil(err)

	bodyRequest = strings.NewReader("abc")
	user, err = decodeProduct(bodyRequest)
	suite.NotEqual(user, suite.productEntity)
	suite.NotNil(err)
}

func (suite *ProductHandlerTestSuite) TestCreate() {
	endpoint := "/api/product/create"
	testCases := []struct {
		name          string
		productEntity entity.Product
		bodyReader    io.Reader
		err           error
		httpCode      int
	}{
		{
			name:          "Test Case 1 | Success Register | 200",
			productEntity: suite.productEntity,
			bodyReader:    strings.NewReader(string(suite.productByte)),
			httpCode:      http.StatusCreated,
			err:           nil,
		},
		{
			name:          "Test Case 2 | Failed Decode | 400",
			productEntity: entity.Product{},
			bodyReader:    strings.NewReader(""),
			httpCode:      http.StatusBadRequest,
			err:           errors.New("could not decode user"),
		},
		{
			name:          "Test Case 3 | Failed Register | 500",
			productEntity: suite.productEntity,
			bodyReader:    strings.NewReader(string(suite.productByte)),
			httpCode:      http.StatusInternalServerError,
			err:           errors.New(repository.ErrUnexpectedCreateUser),
		},
	}

	router := httprouter.New()
	router.POST(endpoint, middleware.None(suite.productHandler.Create))

	for _, tc := range testCases {
		req, _ := http.NewRequest(http.MethodPost, endpoint, tc.bodyReader)
		req.Header.Set("Content-Type", "application/json")

		suite.mockProductService.On("Create", req.Context(), tc.productEntity).Return(tc.err).Once()

		responseRecorder := httptest.NewRecorder()

		router.ServeHTTP(responseRecorder, req)

		suite.Equal(tc.httpCode, responseRecorder.Code)
	}
}

func (suite *ProductHandlerTestSuite) TestDelete() {
	endpoint := "/api/product/delete/:id"
	testCases := []struct {
		name     string
		id       string
		err      error
		httpCode int
	}{
		{
			name:     "Test Case 1 | Success Create Product | 200",
			id:       "1",
			httpCode: http.StatusOK,
			err:      nil,
		},
		{
			name:     "Test Case 2 | Invalid ID Request | 400",
			id:       "2xx",
			httpCode: http.StatusBadRequest,
			err:      errors.New("could not decode user"),
		},
		//{
		//	name:     "Test Case 3 | Record Not Found | 404",
		//	id:       "3",
		//	httpCode: http.StatusNotFound,
		//	err:      errors.New(entity.RecordNotFound),
		//},
		{
			name:     "Test Case 4 | Failed Delete Product | 500",
			id:       "4",
			httpCode: http.StatusInternalServerError,
			err:      errors.New("unexpected delete product"),
		},
	}

	router := httprouter.New()
	router.DELETE(endpoint, middleware.None(suite.productHandler.Delete))

	for _, tc := range testCases {
		req, _ := http.NewRequest(http.MethodDelete, "/api/product/delete/"+tc.id, nil)

		req.Header.Set("Content-Type", "application/json")

		id := handler.GetIDByParams(httprouter.Params{httprouter.Param{Key: "id", Value: tc.id}})

		suite.mockProductService.On("Delete", req.Context(), id).Return(tc.err).Once()

		responseRecorder := httptest.NewRecorder()

		router.ServeHTTP(responseRecorder, req)

		suite.Equal(tc.httpCode, responseRecorder.Code)
	}
}

func (suite *ProductHandlerTestSuite) TestDetails() {
	endpoint := "/api/product/get/:id"
	testCases := []struct {
		name          string
		id            string
		productEntity entity.Product
		err           error
		httpCode      int
	}{
		{
			name:          "Test Case 1 | Success Get Details | 200",
			id:            "1",
			productEntity: suite.productEntity,
			httpCode:      http.StatusOK,
			err:           nil,
		},
		{
			name:          "Test Case 2 | Invalid ID Request | 400",
			id:            "2xx",
			httpCode:      http.StatusBadRequest,
			productEntity: entity.Product{},
			err:           errors.New("could not decode user"),
		},
		//{
		//	name:     "Test Case 3 | Record Not Found | 404",
		//	id:       "3",
		//	httpCode: http.StatusNotFound,
		//	err:      errors.New(entity.RecordNotFound),
		//},
		{
			name:          "Test Case 4 | Failed Get Product | 500",
			id:            "4",
			httpCode:      http.StatusInternalServerError,
			productEntity: entity.Product{},
			err:           errors.New("unexpected get product"),
		},
	}

	router := httprouter.New()
	router.GET(endpoint, middleware.None(suite.productHandler.Detail))

	for _, tc := range testCases {
		req, _ := http.NewRequest(http.MethodGet, "/api/product/get/"+tc.id, nil)

		req.Header.Set("Content-Type", "application/json")

		id := handler.GetIDByParams(httprouter.Params{httprouter.Param{Key: "id", Value: tc.id}})

		suite.mockProductService.On("Detail", req.Context(), id).Return(tc.productEntity, tc.err).Once()

		responseRecorder := httptest.NewRecorder()

		router.ServeHTTP(responseRecorder, req)

		suite.Equal(tc.httpCode, responseRecorder.Code)
	}
}

func (suite *ProductHandlerTestSuite) TestList() {
	endpoint := "/api/product/get"
	testCases := []struct {
		name            string
		productEntities []entity.Product
		options         utility.ListOptions
		err             error
		httpCode        int
	}{
		{
			name:            "Test Case 1 | Success Get Details | 201",
			productEntities: []entity.Product{suite.productEntity},
			options:         utility.NewListOptions(),
			httpCode:        http.StatusOK,
			err:             nil,
		},
		{
			name:            "Test Case 2 | Failed Get Products | 500",
			productEntities: []entity.Product{},
			options:         utility.NewListOptions(),
			httpCode:        http.StatusInternalServerError,
			err:             errors.New("unexpected get products"),
		},
	}

	router := httprouter.New()
	router.GET(endpoint, middleware.None(suite.productHandler.List))

	for _, tc := range testCases {
		req, _ := http.NewRequest(http.MethodGet, endpoint, nil)
		req.Header.Set("Content-Type", "application/json")

		suite.mockProductService.On("List", req.Context(), tc.options).Return(tc.productEntities, tc.err).Once()

		responseRecorder := httptest.NewRecorder()
		router.ServeHTTP(responseRecorder, req)

		suite.Equal(tc.httpCode, responseRecorder.Code)
	}
}

func (suite *ProductHandlerTestSuite) TestUpdate() {
	endpoint := "/api/product/update/:id"
	testCases := []struct {
		name       string
		id         string
		product    entity.Product
		bodyReader io.Reader
		err        error
		httpCode   int
	}{
		{
			name:       "Test Case 1 | Success Update Product | 200",
			id:         "1",
			product:    suite.productEntity,
			bodyReader: strings.NewReader(string(suite.productByte)),
			httpCode:   http.StatusOK,
			err:        nil,
		},
		{
			name:       "Test Case 2 | Invalid ID Request | 400",
			id:         "2xx",
			product:    suite.productEntity,
			bodyReader: strings.NewReader(string(suite.productByte)),
			httpCode:   http.StatusBadRequest,
			err:        errors.New("invalid ID request"),
		},
		{
			name:       "Test Case 3 | Failed Decode Product | 400",
			id:         "3",
			product:    suite.productEntity,
			bodyReader: strings.NewReader(""),
			httpCode:   http.StatusBadRequest,
			err:        errors.New("invalid ID request"),
		},
		//{
		//	name:       "Test Case 4 | Failed Record Not Found | 404",
		//	id:         "4",
		//	product:    suite.productEntity,
		//	bodyReader: strings.NewReader(string(suite.productByte)),
		//	httpCode:   http.StatusNotFound,
		//	err:        errors.New(entity.RecordNotFound),
		//},
		{
			name:       "Test Case 5 | Failed Update Product | 500",
			id:         "5",
			product:    suite.productEntity,
			bodyReader: strings.NewReader(string(suite.productByte)),
			httpCode:   http.StatusInternalServerError,
			err:        errors.New("unexpected update product"),
		},
	}

	router := httprouter.New()
	router.PUT(endpoint, middleware.None(suite.productHandler.Update))

	for _, tc := range testCases {
		req, _ := http.NewRequest(http.MethodPut, "/api/product/update/"+tc.id, tc.bodyReader)

		req.Header.Set("Content-Type", "application/json")

		id := handler.GetIDByParams(httprouter.Params{httprouter.Param{Key: "id", Value: tc.id}})

		suite.mockProductService.On("Update", req.Context(), id, tc.product).Return(tc.err).Once()

		responseRecorder := httptest.NewRecorder()

		router.ServeHTTP(responseRecorder, req)

		suite.Equal(tc.httpCode, responseRecorder.Code)
	}
}

func TestUserHandler(t *testing.T) {
	suite.Run(t, new(ProductHandlerTestSuite))
}
