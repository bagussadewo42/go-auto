package product_handler

import (
	"encoding/json"
	"errors"
	"github.com/julienschmidt/httprouter"
	"gitlab.com/baguss42/go-auto/entity"
	"gitlab.com/baguss42/go-auto/handler"
	ps "gitlab.com/baguss42/go-auto/service/product_service"
	list "gitlab.com/baguss42/go-auto/utility"
	"io"
	"net/http"
)

type ProductHandler struct {
	Service ps.ProductServiceInterface
}

func decodeProduct(r io.Reader) (entity.Product, error) {
	var product entity.Product
	decoder := json.NewDecoder(r)
	if err := decoder.Decode(&product); err != nil {
		return product, errors.New("could not decode body")
	}

	return product, nil
}

func (p *ProductHandler) Create(w http.ResponseWriter, r *http.Request, _ httprouter.Params) (int, error) {
	product, err := decodeProduct(r.Body)
	if err != nil {
		return handler.WriteBadRequestError(w, err)
	}

	err = p.Service.Create(r.Context(), product)
	if err != nil {
		return handler.WriteInternalServerError(w, err)
	}

	return handler.WriteCreated(w, "created")
}

func (p *ProductHandler) Delete(w http.ResponseWriter, r *http.Request, params httprouter.Params) (int, error) {
	id := handler.GetIDByParams(params)
	if id == 0 {
		return handler.WriteBadRequestError(w, errors.New("invalid ID request"))
	}

	err := p.Service.Delete(r.Context(), id)
	if err != nil {
		//if err.Error() == entity.MongoNotFound {
		//	return handler.WriteNotFoundError(w, entity.ErrRecordNotFound)
		//}
		return handler.WriteInternalServerError(w, err)
	}

	return handler.WriteSuccess(w, "success")
}

func (p *ProductHandler) Detail(w http.ResponseWriter, r *http.Request, params httprouter.Params) (int, error) {
	id := handler.GetIDByParams(params)
	if id == 0 {
		return handler.WriteBadRequestError(w, errors.New("invalid ID request"))
	}

	product, err := p.Service.Detail(r.Context(), id)
	if err != nil {
		//if err.Error() == entity.MongoNotFound {
		//	return handler.WriteNotFoundError(w, entity.ErrRecordNotFound)
		//}
		return handler.WriteInternalServerError(w, err)
	}

	return handler.WriteSuccess(w, product)
}

func (p *ProductHandler) List(w http.ResponseWriter, r *http.Request, _ httprouter.Params) (int, error) {
	options := list.NewListOptionsQueryParams(r.URL.Query())

	products, err := p.Service.List(r.Context(), options)
	if err != nil {
		return handler.WriteInternalServerError(w, err)
	}

	return handler.WriteSuccess(w, products)
}

func (p *ProductHandler) Update(w http.ResponseWriter, r *http.Request, params httprouter.Params) (int, error) {
	id := handler.GetIDByParams(params)
	if id == 0 {
		return handler.WriteBadRequestError(w, errors.New("invalid ID request"))
	}

	product, err := decodeProduct(r.Body)
	if err != nil {
		return handler.WriteBadRequestError(w, err)
	}

	err = p.Service.Update(r.Context(), id, product)
	if err != nil {
		//if err.Error() == entity.MongoNotFound {
		//	return handler.WriteNotFoundError(w, entity.ErrRecordNotFound)
		//}
		return handler.WriteInternalServerError(w, err)
	}

	return handler.WriteSuccess(w, "success")
}
