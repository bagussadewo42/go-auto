package handler

import (
	"context"
	"encoding/json"
	"github.com/julienschmidt/httprouter"
	"net/http"
	"net/url"
	"strconv"
	"time"
)

type ResponsePayload struct {
	Data interface{} `json:"data"`
	Meta Meta        `json:"meta"`
}

type Meta map[string]int

func NewResponse(data interface{}, httpCode int) *ResponsePayload {
	return &ResponsePayload{
		Data: data,
		Meta: map[string]int{
			"meta": httpCode,
		},
	}
}

func Index(w http.ResponseWriter, _ *http.Request, _ httprouter.Params) (int, error) {
	_, _ = w.Write([]byte("ok"))
	return http.StatusOK, nil
}

func (r *ResponsePayload) Write(w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(r.Meta["meta"])
	json.NewEncoder(w).Encode(r.Data)
}

func WriteSuccess(w http.ResponseWriter, payload interface{}) (int, error) {
	resp := NewResponse(payload, http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(resp)
	return http.StatusOK, nil
}

func WriteCreated(w http.ResponseWriter, payload interface{}) (int, error) {
	resp := NewResponse(payload, http.StatusCreated)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(resp)
	return http.StatusCreated, nil
}

func WriteNotFoundError(w http.ResponseWriter, err error) (int, error) {
	resp := NewResponse(err.Error(), http.StatusNotFound)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusNotFound)
	json.NewEncoder(w).Encode(resp)
	return http.StatusNotFound, err
}

func WriteInternalServerError(w http.ResponseWriter, err error) (int, error) {
	resp := NewResponse(err.Error(), http.StatusInternalServerError)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusInternalServerError)
	json.NewEncoder(w).Encode(resp)
	return http.StatusInternalServerError, err
}

func WriteUnauthorizedError(w http.ResponseWriter, err error) (int, error) {
	resp := NewResponse(err.Error(), http.StatusUnauthorized)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusUnauthorized)
	json.NewEncoder(w).Encode(resp)
	return http.StatusUnauthorized, err
}

func WriteForbiddenError(w http.ResponseWriter, payload interface{}) {
	resp := NewResponse(payload, http.StatusForbidden)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusInternalServerError)
	json.NewEncoder(w).Encode(resp)
}

func WriteBadRequestError(w http.ResponseWriter, err error) (int, error) {
	resp := NewResponse(err.Error(), http.StatusBadRequest)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusBadRequest)
	json.NewEncoder(w).Encode(resp)
	return http.StatusBadRequest, err
}

func GetUserIDFromContext(ctx context.Context) uint {
	uidString, ok := ctx.Value("userID").(string)
	if !ok {
		return uint(0)
	}
	if uidString != "" {
		uid, err := strconv.Atoi(uidString)
		if err != nil {
			return uint(0)
		}
		return uint(uid)
	}
	return uint(0)
}

func SetCookieIfNotExist(r *http.Request, w http.ResponseWriter, name, value string, expires time.Time) {
	c := &http.Cookie{}

	if storedCookie, _ := r.Cookie(name); storedCookie != nil {
		c = storedCookie
	}

	if c.Value == "" {
		SetCookie(w, name, value, expires)
		return
	}

	http.SetCookie(w, c)
}

func SetCookie(w http.ResponseWriter, name, value string, expires time.Time) {
	c := &http.Cookie{}

	c.Name = name
	c.Value = value
	c.Expires = expires
	c.HttpOnly = true

	http.SetCookie(w, c)
}

func UnsetCookie(w http.ResponseWriter, name string) {
	c := &http.Cookie{
		Name:     name,
		Value:    "",
		Expires:  time.Now().Add(-time.Hour),
		HttpOnly: true,
	}

	http.SetCookie(w, c)
}

func GetIDByParams(params httprouter.Params) uint {
	idParams := params.ByName("id")
	if idParams != "" {
		uid, err := strconv.Atoi(idParams)
		if err != nil {
			return uint(0)
		}
		return uint(uid)
	}
	return uint(0)
}

func GetParamStrByKey(values url.Values, key string) string {
	if val := values.Get(key); val != "" {
		return val
	}
	return ""
}

func GetParamInt64ByKey(values url.Values, key string) int64 {
	val := GetParamStrByKey(values, key)
	if res, err := strconv.ParseInt(val, 10, 64); err == nil {
		return res
	}
	return 0
}
