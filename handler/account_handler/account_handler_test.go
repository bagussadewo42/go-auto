package account_handler

import (
	"context"
	"encoding/json"
	"errors"
	"github.com/joho/godotenv"
	"github.com/julienschmidt/httprouter"
	"github.com/stretchr/testify/suite"
	"gitlab.com/baguss42/go-auto/entity"
	"gitlab.com/baguss42/go-auto/instrument/jwt"
	"gitlab.com/baguss42/go-auto/middleware"
	"gitlab.com/baguss42/go-auto/repository"
	"gitlab.com/baguss42/go-auto/testdata/fixture"
	mockService "gitlab.com/baguss42/go-auto/testdata/mock/service"
	"io"
	"log"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"strconv"
	"strings"
	"testing"
)

type UserHandlerTestSuite struct {
	suite.Suite
	mockAccountService *mockService.AccountServiceMock
	accountHandler     AccountHandler
	userEntity         entity.User
	userByte           []byte
}

func (suite *UserHandlerTestSuite) SetupTest() {
	suite.mockAccountService = &mockService.AccountServiceMock{}
	suite.accountHandler = AccountHandler{Service: suite.mockAccountService}
	suite.userEntity = (&fixture.UserFixture{}).Build()

	var err error
	suite.userByte, err = json.Marshal(suite.userEntity)
	if err != nil {
		log.Fatalf("could not encode user")
	}
	middleware.Load()
}

func (suite *UserHandlerTestSuite) TestDecodeUser() {
	bodyRequest := strings.NewReader(string(suite.userByte))
	user, err := DecodeUser(bodyRequest)
	suite.Equal(user, suite.userEntity)
	suite.Nil(err)

	bodyRequest = strings.NewReader("abc")
	user, err = DecodeUser(bodyRequest)
	suite.NotEqual(user, suite.userEntity)
	suite.NotNil(err)
}

func (suite *UserHandlerTestSuite) TestRegister() {
	endpoint := "/api/account/register"
	testCases := []struct {
		name       string
		userEntity entity.User
		bodyReader io.Reader
		err        error
		httpCode   int
	}{
		{
			name:       "Test Case 1 | Success Register | 200",
			userEntity: suite.userEntity,
			bodyReader: strings.NewReader(string(suite.userByte)),
			httpCode:   200,
			err:        nil,
		},
		{
			name:       "Test Case 2 | Failed Decode | 500",
			userEntity: entity.User{},
			bodyReader: strings.NewReader(""),
			httpCode:   500,
			err:        errors.New("could not decode user"),
		},
		{
			name:       "Test Case 3 | Failed Register | 500",
			userEntity: suite.userEntity,
			bodyReader: strings.NewReader(string(suite.userByte)),
			httpCode:   500,
			err:        errors.New(repository.ErrUnexpectedCreateUser),
		},
	}

	router := httprouter.New()
	router.POST(endpoint, middleware.Apply(suite.accountHandler.Register))

	for _, tc := range testCases {
		req, _ := http.NewRequest(http.MethodPost, endpoint, tc.bodyReader)
		req.Header.Set("Content-Type", "application/json")

		suite.mockAccountService.On("Register", tc.userEntity).Return(tc.err).Once()

		responseRecorder := httptest.NewRecorder()

		router.ServeHTTP(responseRecorder, req)

		suite.Equal(tc.httpCode, responseRecorder.Code)
	}
}

func (suite *UserHandlerTestSuite) TestLogin() {
	endpoint := "/api/account/login"
	testCases := []struct {
		name     string
		email    string
		password string
		token    string
		errAuth  error
		httpCode int
	}{
		{
			name:     "Test Case 1 | Success Login | 200",
			email:    "foo@bar.com",
			password: "12345",
			token:    "anything",
			httpCode: 200,
			errAuth:  nil,
		},
		{
			name:     "Test Case 2 | Failed Login | 401",
			email:    "foo@bar.com",
			password: "",
			token:    "",
			httpCode: 401,
			errAuth:  errors.New("unauthorized login"),
		},
	}
	router := httprouter.New()

	router.POST(endpoint, middleware.Apply(suite.accountHandler.Login))

	for _, tc := range testCases {
		form := url.Values{
			"email":    []string{tc.email},
			"password": []string{tc.password},
		}

		req, _ := http.NewRequest(http.MethodPost, endpoint, nil)
		req.Form = form

		suite.mockAccountService.On("Login", tc.email, tc.password).Return(tc.token, tc.errAuth).Once()

		responseRecorder := httptest.NewRecorder()

		router.ServeHTTP(responseRecorder, req)

		suite.Equal(tc.httpCode, responseRecorder.Code)
	}
}

func (suite *UserHandlerTestSuite) TestProfile() {
	dir, _ := os.Getwd()
	s := strings.SplitAfter(dir, "go-auto")
	err := godotenv.Load(s[0] + "/env.sample")
	if err != nil {
		log.Println(err)
	}
	endpoint := "/api/account/profile"
	testCases := []struct {
		name       string
		userEntity entity.User
		httpCode   int
		err        error
		isErrToken bool
	}{
		{
			name:       "Test Case 1 | Success Get Profile | 200",
			userEntity: suite.userEntity,
			httpCode:   200,
			err:        nil,
			isErrToken: false,
		},
		{
			name:       "Test Case 3 | Failed Verify Token | 401",
			userEntity: suite.userEntity,
			httpCode:   401,
			err:        nil,
			isErrToken: true,
		},
	}
	router := httprouter.New()

	router.GET(endpoint, middleware.Apply(middleware.Auth(suite.accountHandler.Profile)))

	for _, tc := range testCases {
		var token string
		if !tc.isErrToken {
			token, err = jwt.ClaimToken(uint64(tc.userEntity.ID))
			if err != nil {
				log.Println(err)
			}
		}

		req, _ := http.NewRequest(http.MethodGet, endpoint, nil)
		if tc.userEntity.ID != 0 {
			req.AddCookie(&http.Cookie{Name: jwt.COOKIE_NAME_JWT, Value: token})
		}
		ctx := req.Context()

		ctx = context.WithValue(ctx, "userID", strconv.Itoa(int(tc.userEntity.ID)))

		suite.mockAccountService.On("Profile", ctx).Return(tc.userEntity, tc.err).Once()

		responseRecorder := httptest.NewRecorder()

		router.ServeHTTP(responseRecorder, req)

		suite.Equal(tc.httpCode, responseRecorder.Code)
	}
}

func (suite *UserHandlerTestSuite) TestLogout() {
	endpoint := "/api/account/logout"
	testCases := []struct {
		name       string
		userEntity entity.User
		httpCode   int
		err        error
	}{
		{
			name:       "Test Case 1 | Success Get Profile | 200",
			userEntity: suite.userEntity,
			httpCode:   200,
			err:        nil,
		},
		{
			name:       "Test Case 2 | Failed Get Cookie | 401",
			userEntity: entity.User{},
			httpCode:   401,
			err:        nil,
		},
	}
	router := httprouter.New()

	router.POST(endpoint, middleware.Apply(middleware.Auth(suite.accountHandler.Logout)))

	for _, tc := range testCases {
		req, _ := http.NewRequest(http.MethodPost, endpoint, nil)
		if tc.userEntity.ID != 0 {
			var token string
			token, err := jwt.ClaimToken(uint64(tc.userEntity.ID))
			if err != nil {
				log.Println(err)
			}
			req.AddCookie(&http.Cookie{Name: jwt.COOKIE_NAME_JWT, Value: token})
		}

		responseRecorder := httptest.NewRecorder()

		router.ServeHTTP(responseRecorder, req)

		suite.Equal(tc.httpCode, responseRecorder.Code)
	}
}

func (suite *UserHandlerTestSuite) TestVerifyEmail() {
	endpoint := "/api/account/verify/email"
	testCases := []struct {
		name             string
		email            string
		verificationCode string
		httpCode         int
		err              error
	}{
		{
			name:             "Test Case 1 | Success Verify Email | 200",
			email:            "foo@bar.com",
			verificationCode: "123",
			httpCode:         200,
			err:              nil,
		},
		{
			name:             "Test Case 2 | Failed Verify Email | 400",
			email:            "foo@bar.com",
			verificationCode: "123xxx",
			httpCode:         400,
			err:              errors.New("account not found"),
		},
	}
	router := httprouter.New()

	router.POST(endpoint, middleware.Apply(suite.accountHandler.VerifyEmail))

	for _, tc := range testCases {
		form := url.Values{
			"email":             []string{tc.email},
			"verification_code": []string{tc.verificationCode},
		}
		req, _ := http.NewRequest(http.MethodPost, endpoint, nil)
		req.Form = form

		suite.mockAccountService.On("VerifyEmail", tc.email, tc.verificationCode).Return(tc.err).Once()

		responseRecorder := httptest.NewRecorder()

		router.ServeHTTP(responseRecorder, req)

		suite.Equal(tc.httpCode, responseRecorder.Code)
	}
}

func (suite *UserHandlerTestSuite) TestResendVerifyEmail() {
	endpoint := "/api/account/resend_verify/email"
	testCases := []struct {
		name     string
		email    string
		httpCode int
		err      error
	}{
		{
			name:     "Test Case 1 | Success Resend Verify Email Code | 200",
			email:    "foo@bar.com",
			httpCode: 200,
			err:      nil,
		},
		{
			name:     "Test Case 2 | Failed Resend Verify Email Code | 400",
			email:    "foo@bar.com",
			httpCode: 400,
			err:      errors.New("account not found"),
		},
	}
	router := httprouter.New()

	router.POST(endpoint, middleware.Apply(suite.accountHandler.ResendVerifyEmail))

	for _, tc := range testCases {
		form := url.Values{
			"email": []string{tc.email},
		}
		req, _ := http.NewRequest(http.MethodPost, endpoint, nil)
		req.Form = form

		suite.mockAccountService.On("ResendVerifyEmail", tc.email).Return(tc.err).Once()

		responseRecorder := httptest.NewRecorder()

		router.ServeHTTP(responseRecorder, req)

		suite.Equal(tc.httpCode, responseRecorder.Code)
	}
}

func TestUserHandler(t *testing.T) {
	suite.Run(t, new(UserHandlerTestSuite))
}
