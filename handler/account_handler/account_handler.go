package account_handler

import (
	"encoding/json"
	"errors"
	"github.com/julienschmidt/httprouter"
	"gitlab.com/baguss42/go-auto/entity"
	"gitlab.com/baguss42/go-auto/handler"
	"gitlab.com/baguss42/go-auto/instrument/jwt"
	"gitlab.com/baguss42/go-auto/service/account_service"
	"io"
	"net/http"
	"time"
)

type AccountHandler struct {
	Service account_service.AccountServiceInterface
}

func DecodeUser(r io.Reader) (entity.User, error) {
	var user entity.User
	decoder := json.NewDecoder(r)
	if err := decoder.Decode(&user); err != nil {
		return user, errors.New("could not decode body")
	}

	return user, nil
}

func (u *AccountHandler) Register(w http.ResponseWriter, r *http.Request, _ httprouter.Params) (int, error) {
	user, err := DecodeUser(r.Body)
	if err != nil {
		return handler.WriteInternalServerError(w, err)
	}

	err = u.Service.Register(user)
	if err != nil {
		return handler.WriteInternalServerError(w, err)
	}

	return handler.WriteSuccess(w, "success")
}

func (u *AccountHandler) Login(w http.ResponseWriter, r *http.Request, _ httprouter.Params) (int, error) {
	email := r.FormValue("email")
	password := r.FormValue("password")

	token, err := u.Service.Login(email, password)
	if err != nil {
		return handler.WriteUnauthorizedError(w, err)
	}

	handler.SetCookie(w, jwt.COOKIE_NAME_JWT, token, time.Now().Add(time.Hour*24))

	return handler.WriteSuccess(w, token)
}

func (u *AccountHandler) Profile(w http.ResponseWriter, r *http.Request, _ httprouter.Params) (int, error) {
	user := u.Service.Profile(r.Context())

	return handler.WriteSuccess(w, user)
}

func (u *AccountHandler) VerifyEmail(w http.ResponseWriter, r *http.Request, _ httprouter.Params) (int, error) {
	email := r.FormValue("email")
	verificationCode := r.FormValue("verification_code")

	err := u.Service.VerifyEmail(email, verificationCode)
	if err != nil {
		return handler.WriteBadRequestError(w, err)
	}

	return handler.WriteSuccess(w, "verify success")
}

func (u *AccountHandler) ResendVerifyEmail(w http.ResponseWriter, r *http.Request, _ httprouter.Params) (int, error) {
	email := r.FormValue("email")

	err := u.Service.ResendVerifyEmail(email)
	if err != nil {
		return handler.WriteBadRequestError(w, err)
	}

	return handler.WriteSuccess(w, "verification code successfully send")
}

func (u *AccountHandler) Logout(w http.ResponseWriter, r *http.Request, _ httprouter.Params) (int, error) {
	handler.UnsetCookie(w, jwt.COOKIE_NAME_JWT)

	return handler.WriteSuccess(w, "success logout")
}
